package co.istad.diresources;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DiResourcesApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(DiResourcesApplication.class, args);
    }

    @Value("${spring.application.name}")
    private String appName;

    @Value("${server.port}")
    private Integer appPort;

    @Value("${user.username}")
    private String username;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Spring boot started!");
        System.out.println("--------------------");
        System.out.println(appName);
        System.out.println(appPort);
        System.out.println(username);
    }

}
